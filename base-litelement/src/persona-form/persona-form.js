import { LitElement, html} from 'lit-element';

class PersonaForm extends LitElement {
    
    static get properties(){
        return {
            person: {type: Object},
            editingPerson: {type: Boolean}
        }
    }

    constructor(){
        super();

        this.initFormData();
    }
    
    render(){
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"  crossorigin="anonymous" />
            <div>
                <form>
                    <div class="form-group">
                        <label>Nombre Completo</label>
                        <input type="text" @input="${this.updateName}" .value="${this.person.name}" ?disabled="${this.editingPerson}" id="personFormName" class="form-control" placeholder="Nombre Completo" />
                    </div>
                    <div class="form-group">
                        <label>Perfil</label>
                        <textarea @input="${this.updateProfile}" .value="${this.person.profile}" class="form-control" placeholder="Perfil" rows="5"></textarea>
                    </div>
                    <div class="form-group">
                        <label>Años en la empresa</label>
                        <input type="text" @input="${this.updateYearsInCompany}" .value="${this.person.yearsInCompany}" class="form-control" placeholder="Años en la empresa" />
                    </div>
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Reset</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
        `;
    }

    updateName(e){
        console.log("updateName");
        console.log("Actualizando la propiedad name con el valor "+ e.target.value);
        this.person.name=e.target.value;
    }

    updateProfile(e){
        console.log("updateProfile");
        console.log("Actualizando la propiedad profile con el valor "+ e.target.value);
        this.person.profile=e.target.value;
    }

    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        console.log("Actualizando la propiedad yearsInCompany con el valor "+ e.target.value);
        this.person.yearsInCompany=e.target.value;
    }

    goBack(e){
        console.log("goBack");
        e.preventDefault();

        //inicializamos el formulario
        this.initFormData();

        this.dispatchEvent(new CustomEvent("persona-form-reset-event",{}));
    }

    storePerson(e){
        console.log("storePerson");
        e.preventDefault();

        this.person.photo = {
            "src": "./img/face.jpg",
            "alt": "Texto alternativo"
        };

        console.log("La propiedad name vale "+this.person.name);
        console.log("La propiedad perfil vale "+this.person.profile);
        console.log("La propiedad yearsInCompany vale "+this.person.yearsInCompany);

        this.dispatchEvent(new CustomEvent("persona-form-store-event", {
            detail: {
                person: {
                    name: this.person.name,
                    profile: this.person.profile,
                    yearsInCompany: this.person.yearsInCompany,
                    photo: this.person.photo
                },
                editingPerson: this.editingPerson
            }
        }
        ));
    }

    initFormData(){
        console.log("initFormData");

        this.person = {};
        this.person.name = "";
        this.person.profile = "";
        this.person.yearsInCompany = "";

        this.editingPerson = false;
    }
}

customElements.define('persona-form', PersonaForm);