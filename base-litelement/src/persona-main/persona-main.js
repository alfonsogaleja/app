import { LitElement, html,css} from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';

class PersonaMain extends LitElement {

    static get styles(){
        return css`
            :host{
                all: initial;
            }
        `;
    }

    static get properties(){
        return {
            people: {type:Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor(){
        super();

        this.people = [
            {
                name: "Ellen Ripley", 
                yearsInCompany: 5,
                photo: {
                    "src": "./img/face.jpg",
                    "alt": "Ellen Ripley"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."
            },{
                name: "Bruce Banner",
                yearsInCompany: 10,
                photo: {
                    "src": "./img/face.jpg",
                    "alt": "Bruce Banner"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."
            },{
                name: "Eowyn",
                yearsInCompany: 15,
                photo: {
                    "src": "./img/face.jpg",
                    "alt": "Eowyn"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."

            },{
                name: "Bob Esponja",
                yearsInCompany: 9,
                photo: {
                    "src": "./img/face.jpg",
                    "alt": "Bob Esponja"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."

            },{
                name: "Tyrion Lanister",
                yearsInCompany: 1,
                photo: {
                    "src": "./img/face.jpg",
                    "alt": "Tyrion Lanister"
                },
                profile: "Lorem ipsum dolor sit amet, consectetur adipisici elit, sed eiusmod tempor incidunt ut labore et dolore magna aliqua. Ut enim ad minim veniam."

            }
        ];

        this.showPersonForm = false;
    }

    render(){
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"  crossorigin="anonymous" />
            <h2 class="text-center">Personas</h2>
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                    ${this.people.map(
                        person => html`<persona-ficha-listado  
                                            @info-person="${this.infoPerson}"
                                            @delete-person="${this.deletePerson}"
                                            fname="${person.name}" 
                                            fyearsInCompany="${person.yearsInCompany}" 
                                            fprofile="${person.profile}"  
                                            fphoto='{"src": "${person.photo.src}", "alt": "${person.photo.alt}"}'>
                                        </persona-ficha-listado>`
                )}
                </div>
            </div>
            <div class="row">
                <persona-form @persona-form-store-event=${this.storePerson} @persona-form-reset-event="${this.formReset}" id="peopleForm" class="d-none rounded border border-primary"></persona-form>
            </div>
        `;
    }

    deletePerson(e){
        console.log("deletePerson en persona-main");
        
        this.people = this.people.filter(
            person => person.name != e.detail.name
        );

        console.log(this.people);
    }

    infoPerson(e){
        console.log("infoPerson en persona-main");

        let chosenPerson = this.people.filter(
            person => person.name === e.detail.name
        );

        console.log(chosenPerson[0]);
        this.shadowRoot.getElementById("peopleForm").person = chosenPerson[0];
        this.shadowRoot.getElementById("peopleForm").editingPerson = true;

        this.showPersonForm = true;
    }

    updated(changedProperties){
        console.log("updated");

        if(changedProperties.has("showPersonForm")){
            console.log("Se ha cambiado la propiedad showPersonForm");

            if(this.showPersonForm === true){
                this.showPersonFormData();
            }else{
                this.showPersonList();
            }
        }

        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-main");

            this.dispatchEvent(
                new CustomEvent("update-people-event", {
                    detail: {
                        people: this.people
                    }
                })
            );
        }
    }

    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando el listado de personas");
        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleForm").classList.add("d-none");
    }

    showPersonFormData(){
        console.log("showPersonForm");
        console.log("Mostrando el formulario de personas");
        this.shadowRoot.getElementById("peopleForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    formReset(){
        console.log("formReset");
        console.log("Se ha cerrado el formulario de persona-form");

        this.showPersonForm=false;
    }

    storePerson(e){
        console.log("storePerson");
        console.log("Se va a guardar la persona en persona-form");
        console.log("Modo editingPerson = " + e.detail.editingPerson);

        if(e.detail.editingPerson === true){
            console.log("Se va a actualizar la persona de nombre " + e.detail.person.name);

            this.people = this.people.map(
                person => person.name === e.detail.person.name
                    ? person = e.detail.person : person       
            );

            // let indexOfPerson = 
            //     this.people.findIndex(
            //         person => person.name === e.detail.person.name
            //     );

            // if (indexOfPerson >=0 ) {
            //     console.log("Persona encontrada");
            //     this.people[indexOfPerson]=e.detail.person;
            // }

        }else{
            console.log("Se va a almacenar una persona nueva de nombre " + e.detail.person.name);
            //this.people.push(e.detail.person);
            this.people = [...this.people, e.detail.person];
            console.log("Persona nueva almacenada");
        }

        console.log("proceso terminado");

        this.showPersonForm=false;
    }

}

customElements.define('persona-main', PersonaMain);