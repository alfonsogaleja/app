import { LitElement, html} from 'lit-element';

class PersonaFichaListado extends LitElement {
    
    static get properties(){ 
        return {
            fname: {type: String},
            fyearsInCompany: {type: Number},
            fphoto: {type: Object},
            fprofile: {type: String}
        };
    }

    constructor(){
        super();
    }

    render(){
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"  crossorigin="anonymous" />
            <div class="card offset-1">
                <img src="${this.fphoto.src}" alt="${this.fphoto.alt}" height="200" width="100%" class="card-img-top"/>
                <div class="card-body">
                    <h5 class="card-title">${this.fname}</h5>
                    <p class="card-text">${this.fprofile}</p>
                    <ul class="list-group list-group-flush">
                        <li class="list-group-item">${this.fyearsInCompany} años en la empresa</li>
                    </ul>
                </div>
                <div class="card-footer">
                    <button @click="${this.deletePerson}" class="btn btn-danger col-5"><strong>X</strong></button>
                    <button @click="${this.moreInfo}" class="btn btn-info col-5" offset-1><strong>Info</strong></button>
                </div>
            </div>
        `;
    }

    deletePerson(e){
        console.log("deletePerson en persona-ficha-listado");
        console.log("Se va a borrar la persona de nombre " + this.fname);

        this.dispatchEvent(
            new CustomEvent("delete-person", {
                detail: {
                    name: this.fname
                }
            })
        );
    }

    moreInfo(e){
        console.log("moreInfo en persona-ficha-listado");
        console.log("Se va mostrar información de " + this.fname);

        this.dispatchEvent(
            new CustomEvent("info-person", {
                detail: {
                    name: this.fname
                }
            })
        );
    }
}

customElements.define('persona-ficha-listado', PersonaFichaListado);