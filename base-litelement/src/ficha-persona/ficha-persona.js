import { LitElement, html} from 'lit-element';

class FichaPersona extends LitElement {

    static get properties(){
        return {
            name: {type: String},
            yearsInCompany: {type: Number},
            personInfo: {type: String},
            photo: {type: Object}
        }
    }

    constructor(){
        super();

        this.name = "Alfonso Gonzalez";
        this.yearsInCompany = 12;
        this.photo = {
            src: "./img/face.jpg",
            alt: "Foto persona"
        }

        this.updatePersonInfo(this.yearsInCompany);
    }

    updated(changedProperties){
        changedProperties.forEach((oldValue, propName) => {
            console.log("Propiedad " + propName + ", cambia valor, anterior era, "+ oldValue);
        }
        );

        if (changedProperties.has("name")){
            console.log("Propiedad name cambia de valor anterior era " 
                + changedProperties.get("name") + ", nuevo es, " + this.name);
        }

        if (changedProperties.has("yearsInCompany")){
            console.log("Propiedad yearsInCompany cambia de valor anterior era " +
            changedProperties.get("yearsInCompany") + ", nuevo es, " + this.yearsInCompany);
           
            this.updatePersonInfo(this.yearsInCompany);
        }
    }

    updateName(e){
        console.log("updateName");
        this.name = e.target.value;
    }

    updatePersonInfo(value){
        console.log("updatePersonInfo");

        if (value >=7){
            this.personInfo = "lead";
        }else if (value >=5){
            this.personInfo = "senior";
        }else if (value >=3){
            this.personInfo = "team";
        }else{
            this.personInfo = "junior";
        }
    }

    updateYearsInCompany(e){
        console.log("updateYearsInCompany");
        this.yearsInCompany = e.target.value;
    }

    render(){
        return html`
            <div>
                <label for="fname">Nombre completo</label>
                <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"></input>
                <br />
                <label for="age">Años en la empresa</label>
                <input type="text" id="age" name="age" value="${this.yearsInCompany}" @change="${this.updateYearsInCompany}"></input>
                <br />
                <input type="text" id="personInfo" name="personInfo" value="${this.personInfo}" disabled></input>
                <br />
                <img src="${this.photo.src}" height="200" width="300" alt="${this.photo.alt}" />
            </div>
        `;
    }
}

customElements.define('ficha-persona', FichaPersona);