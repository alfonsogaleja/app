import { LitElement, html} from 'lit-element';
import '../persona-header/persona-header.js';
import '../persona-sidebar/persona-sidebar.js';
import '../persona-main/persona-main.js';
import '../persona-footer/persona-footer.js';
import '../persona-stats/persona-stats.js';

class PersonaApp extends LitElement {
    
    static get properties(){
        return {
            people: {type: Array}
        };
    }

    constructor(){
        super();

        this.people = [];
    }

    render(){
        return html`
            <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css" integrity="sha384-JcKb8q3iqJ61gNV9KGb8thSsNjpSL0n8PARn9HuZOnIxN0hoP+VmmDGMN5t9UJ0Z"  crossorigin="anonymous" />
            <persona-header></persona-header>
            <div class="row">
                <persona-sidebar @new-person-event="${this.newPerson}" class="col-2"></persona-sidebar>
                <persona-main @update-people-event="${this.updatePeople}" class="col-10" id="receiver"></persona-main>
            </div>
            <persona-footer></persona-footer>
            <persona-stats @updated-people-stats="${this.updatedPeopleStats}"></persona-stats>
        `;
    }

    newPerson(e){
        console.log("newPerson");

        let personaMain = this.shadowRoot.querySelector("persona-main");
        personaMain.showPersonForm = true;
    }

    updatePeople(e){
        console.log("updatePeople");
        this.people = e.detail.people;
    }

    updated(changedProperties){
        console.log("updated");

        if(changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-app");
            this.shadowRoot.querySelector("persona-stats").people = this.people;
        }
    }

    updatedPeopleStats(e){
        console.log("updatedPeopleStats");

        let personaSidebar = this.shadowRoot.querySelector("persona-sidebar");
        personaSidebar.peopleStats=e.detail.peopleStats;
    }

}

customElements.define('persona-app', PersonaApp);