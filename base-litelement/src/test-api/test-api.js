import { LitElement, html} from 'lit-element';

class TestApi extends LitElement {
    
    static get properties(){
        return {
            movies: {type: Array}
        };
    }

    constructor(){
        super();

        this.movies=[];
        this.getMovieData();
    }
    
    
    render(){
        return html`
            ${this.movies.map(
                movie => 
                    html `<div>La película ${movie.title}, fue dirigida por ${movie.director}.</div>`
            )}
        `;
    }

    getMovieData(){
        console.log("getMovieData");
        console.log("Obteniendo datos de las películas");

        let xhr = new XMLHttpRequest();
        xhr.onload = () => {
            if(xhr.status === 200){
                console.log("Petición completada correctamente. Estado 200");

                let apiResponse = JSON.parse(xhr.responseText);
                this.movies=apiResponse.results;
            }
        }

        xhr.open("GET", "https://swapi.dev/api/films/", true);
        xhr.send();
    }
}

customElements.define('test-api', TestApi);