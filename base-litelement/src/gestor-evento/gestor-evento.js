import { LitElement, html, eventOptions} from 'lit-element';
import '../emisor-evento/emisor-evento.js';
import '../receptor-evento/receptor-evento.js';

class GestorEvento extends LitElement {
    render(){
        return html`
            <h1>Gestor evento</h1>
            <emisor-evento @test-event="${this.processEvent}"></emisor-evento>
            <receptor-evento id="receiver"></receptor-evento>
        `;
    }

    processEvent(e){
        console.log("Capturado el evento del emisor");
        console.log(e.detail);

        var receptor = this.shadowRoot.getElementById("receiver");
        receptor.course = e.detail.course;
        receptor.year = e.detail.year;
    }
}

customElements.define('gestor-evento', GestorEvento);